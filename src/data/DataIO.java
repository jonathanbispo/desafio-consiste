package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DataIO {
	
	public ArrayList<String> readLines(String path){
		ArrayList<String> Lines = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(getClass().getResource(path).getFile()), "UTF-8"));
			while(br.ready()) {
				Lines.add(br.readLine());
			}
			
			br.close();	
		} catch(Exception e) {
			e.printStackTrace();
		}
		return Lines;
	}
	
	public void saveData(String path, String data) {
		try {			
			FileWriter fw = new FileWriter(new File(path));
			fw.write(data);
			fw.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}