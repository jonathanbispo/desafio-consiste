package data;

import java.util.ArrayList;

public class Lines {
	private ArrayList<String> lines;
	private boolean has_header;
	
	public Lines(String path) {
		this.has_header = true;
		this.lines = setLines(path);
		this.cleanLines();
	}
	
	private void cleanLines() {
		ArrayList<String> temp_lines = new ArrayList<String>();
		for(String line : this.lines) {
			//Nome
			String temp_line[] = line.split(";");
			temp_line[0] = temp_line[0].trim().toUpperCase();
			
			//Sobrenome
			String temp_line_two[] = temp_line[1].split(" ");
			
			String sobrenome = "";
			for(String word: temp_line_two) {
				if(word.length() < 1) {
					continue;
				}
				word = word.toLowerCase().trim().toUpperCase();
				sobrenome += word + " ";
			}
			temp_line[1] = sobrenome.trim();
			
			
			if(temp_line.length==4) {
				//Peso
				temp_line[2] = temp_line[2].trim().replace(",", ".");
				
				//Altura
				temp_line[3] = temp_line[3].trim().replace(",", ".");
			}else {
				for(int i = 0; i<4 - temp_line.length;i++)
				temp_line[temp_line.length-1] += ";";
			}
			
			String line_final = "";
			for(String word : temp_line) {
				line_final += word + ";";
			}
			line_final = line_final.substring(0, line_final.length() -1);
			temp_lines.add(line_final);
			this.lines = temp_lines;
		}
	}
	
	public ArrayList<String> getLines(){
		return this.lines;
	}
	
	public ArrayList<String> setLines(String path) {
		DataIO dr = new DataIO();
		return dr.readLines(path);
	}
	
	public String removeHeader() {
		if(this.has_header) {
			has_header = false;
			return this.lines.remove(0);
		}else {
			return "Header j� foi removido";
		}
	}
}
