package pessoa;

import java.text.DecimalFormat;

public class Pessoa {
	private String nome;
	private String sobrenome;
	private float peso;
	private float altura;
	
	public Pessoa(String line) {
		
		String line_splitted[] = line.split(";");
		
		this.nome = line_splitted[0];
		this.sobrenome = line_splitted[1];
		
		if(line_splitted.length == 4){
			this.peso = Float.parseFloat(line_splitted[2]);
			this.altura = Float.parseFloat(line_splitted[3]);
		}else{
			this.peso = -1f;
			this.altura = -1f;
		}
	}
	
	public String calcularIMC() {
		if(this.peso == -1 || this.altura == -1) {
			return "IMC n�o pode ser calculado. N�o possui altura ou peso cadastrado";
		}
		
		DecimalFormat f = new DecimalFormat("#.##");
		
		String total = f.format(this.peso/(this.altura * this.altura));
		
		if(total.split(",").length == 2) {
			if(total.split(",")[1].length() != 2) {
				total+="0";
			}
		}else {
			total+= ",00";
		}
		return total;
	}
	
	public String getNomeCompleto() {
		return this.nome + " " + this.sobrenome;
	}
	
	@Override
	public String toString() {
		return "Nome:" + nome + ", Sobrenome:" + sobrenome + ", Peso:" + peso + ", Altura:" + altura;
	}
	
}
