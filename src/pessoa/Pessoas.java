package pessoa;

import java.util.ArrayList;

import data.DataIO;
import data.Lines;

public class Pessoas {
	private ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
	
	public Pessoas(Lines lines, boolean header){
		if(header) {
			lines.removeHeader();
		}
		
		for(String line: lines.getLines()) {
			Pessoa p = new Pessoa(line);
			this.pessoas.add(p);
		}
	}
	public Pessoas(Lines lines){
		this(lines, false);
	}
	
	public String CalculaIMCS() {
		String txt = "";
		
		for(Pessoa p: this.pessoas){
			txt += p.getNomeCompleto() +" "+ p.calcularIMC() + "\n";
		}
		return txt;
	}
	
	public void printPessoas() {
		for(Pessoa p : pessoas) {
			System.out.println(p.toString());
		}
	}
	
	public void exportIMC(String path) {
		DataIO dio = new DataIO();
		dio.saveData(path, this.CalculaIMCS());
	}
	
}
